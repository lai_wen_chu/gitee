package com.springboot.demo.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@Controller
@RequestMapping("/user")
public class LoginController {
    @PostMapping("/login")
    public String Login(
            @RequestParam("username") String username,
            @RequestParam("pwd") String password,
            Model model, HttpServletRequest request){
        if (!StringUtils.isEmpty(username) && password.equals("123")){
            //登录成功设置session
            request.getSession().setAttribute("username", username);
            model.addAttribute("date",new Date());
            return "redirect:/success.html";
//            return "success";
        }else {
            //登录失败，提示信息
            model.addAttribute("msg", "用户名密码错误！");
            return "pages-signin";
        }
    }
}
