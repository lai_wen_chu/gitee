package com.springboot.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
//@RestController是@Controller和@ResponseBody的结合
public class testController {
    @Autowired
    JdbcTemplate jdbcTemplate;

    @RequestMapping("/bookList")
    public List<Map<String, Object>> bookList(){
        String sql = "select * from book";
        List<Map<String, Object>> list_map = jdbcTemplate.queryForList(sql);
        return list_map;
    }

    @GetMapping("/addBook")
    public String addBook(){
        String sql = "insert into book(id,isbn,bookName,price,stock) value(4,'004','SpringMVC',50,'5')";
        jdbcTemplate.update(sql);
        return "add-ok";
    }

    @GetMapping("/updateBook/{id}")
    public String updateBook(@PathVariable("id") Integer id){
        String sql = "update book set bookName='SpringBoot教程' where id=?";
        jdbcTemplate.update(sql,id);
        return "update-ok";
    }


    @GetMapping("/deleteBook/{id}")
    public String deleteBook(@PathVariable("id") Integer id){
        String sql = "delete from book where id="+id;
        jdbcTemplate.update(sql);
        return "delete-ok";
    }

}
