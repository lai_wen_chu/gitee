package com.springboot.demo.entity;

import lombok.*;

@Data
@NoArgsConstructor//无参构造函数
@AllArgsConstructor//有参构造函数
@Setter//set
@Getter//get
@ToString//toString
public class Book {
    private Integer id;
    private String isbn;
    private String bookName;
    private Integer price;
    private String stock;

}
