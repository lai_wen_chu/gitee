package com.springboot.demo.mapper;

import com.springboot.demo.entity.Book;
import org.springframework.stereotype.Repository;


import java.util.List;
@Repository
public interface BookMapper {

    public List<Book> bookList();
}
