package com.springboot.demo.controller;

import com.springboot.demo.entity.Book;
import com.springboot.demo.mapper.BookMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


import java.util.List;

@RestController
public class BookController {
    @Autowired
    private BookMapper bookMapper;

    @GetMapping(value = "/findAllBook")
    public List<Book> findAllBook(){
        List<Book> bookList = bookMapper.bookList();
        for (Book book : bookList){
            System.out.println(book);
        }
        return bookList;
    }
}
