package com.lwz.demo2;

import java.text.SimpleDateFormat;
import java.util.Date;

//测试线程休眠
public class TestSleep  {


    public static void main(String[] args) {
        Date date = new Date(System.currentTimeMillis());//获取当前系统时间
        int a=10;
        while (a>0){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(new SimpleDateFormat("HH:mm:ss").format(date));
            date = new Date(System.currentTimeMillis());//更新时间
            a--;
        }
    }
}
