package com.lwz.demo2;
//测试守护线程
public class TestDaemon {
    public static void main(String[] args) {
        God god = new God();
        Thread thread = new Thread(god);
        thread.setDaemon(true);//默认为false.是普通线程;true 为设置线程为守护线程
        thread.start();
        You you = new You();
        new Thread(you).start();
    }

}
class God implements Runnable{

    @Override
    public void run() {
        while (true){
            System.out.println("上帝一直守护着你");
        }
    }
}

class You implements Runnable{

    @Override
    public void run() {
        for (int i = 0; i < 36500; i++) {
            System.out.println("快乐的活着"+i);
        }
        System.out.println("======你离开了美好的世界======");
    }
}
