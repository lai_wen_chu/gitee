package com.lwz.demo2;
//测试yield
public class TestYield implements Runnable {
    public static void main(String[] args) {
        TestYield testYield=new TestYield();
        new Thread(testYield,"a").start();
        new Thread(testYield,"b").start();

    }

    public void run() {
        System.out.println(Thread.currentThread().getName()+"线程开始");
        if (Thread.currentThread().getName().equals("a")){
            Thread.yield();
        }
        System.out.println(Thread.currentThread().getName()+"线程结束");
    }
}
