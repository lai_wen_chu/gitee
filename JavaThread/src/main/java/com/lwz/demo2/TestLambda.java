package com.lwz.demo2;
//lambda
public class TestLambda{
    public static void main(String[] args) {
        //使用lambda前提是接口是函数式接口，函数式接口的意思是接口中只能有一个抽象方法
        new Thread(()->{
            for (int i = 0; i < 20; i++) {
                System.out.println("线程1====>"+i);
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 20; i++) {
                    System.out.println("线程2====>"+i);
                }
            }
        }).start();
    }
}
