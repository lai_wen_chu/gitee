package com.lwz.demo2;
//测试线程并发问题，发现线程不安全
public class TestSleep2 implements Runnable {
    public void run() {
        int ticketNum=10;
        while (true){
            if (ticketNum<=0){
                break;
            }
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName()+"抢到了第"+ticketNum--+"张票");

        }
    }

    public static void main(String[] args) {
        TestSleep2 testThread3=new TestSleep2();
        new Thread(testThread3,"小明").start();
        new Thread(testThread3,"小红").start();
        new Thread(testThread3,"小蓝").start();
    }
}
