package com.lwz.demo2;
//观测线程的状态
public class TestState {
    public static void main(String[] args) throws InterruptedException {
        Thread thread=new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 5; i++) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println("线程开始运行了");
            }
        });
        //观测状态
        Thread.State state = thread.getState();//线程开始之前
        System.out.println(state);

        thread.start();
        state = thread.getState();//线程开始之后
        System.out.println(state);

        while (state != Thread.State.TERMINATED) {//只要线程不终止就一直输出线程状态
            Thread.sleep(100);
            state = thread.getState();//更新线程状态
            System.out.println(state);

        }
    }
}
