package com.lwz.demo2;
//测试优先级
//不是优先级高就一定先调用，这都得看cpu的调度
public class TestPriority {
    public static void main(String[] args) {

        //输出主线程的优先级
        System.out.println(Thread.currentThread().getName()+"===>"+Thread.currentThread().getPriority());

        MyPriority myPriority=new MyPriority();

        Thread t1 = new Thread(myPriority);
        Thread t2 = new Thread(myPriority);
        Thread t3 = new Thread(myPriority);
        Thread t4 = new Thread(myPriority);

        //先设置优先级再启动
        t1.start();//不是优先级高就一定先调用，这都得看cpu的调度

        t2.setPriority(2);
        t2.start();

        t3.setPriority(Thread.MAX_PRIORITY);//MAX_PRIORITY=10
        t3.start();

        t4.setPriority(Thread.NORM_PRIORITY);//NORM_PRIORITY=5
        t4.start();


    }



}
class MyPriority implements Runnable{
    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName()+"===>"+Thread.currentThread().getPriority());
    }
}