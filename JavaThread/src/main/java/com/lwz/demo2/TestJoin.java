package com.lwz.demo2;
//测试join方法
//可理解为插队
public class TestJoin implements Runnable {
    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            System.out.println("vip来了"+i);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        TestJoin testJoin = new TestJoin();
        Thread thread = new Thread(testJoin);
        thread.start();

        for (int i = 0; i < 200; i++) {
            if (i==50){
                thread.join();
            }
            System.out.println("主线程"+i);
        }
    }
}
