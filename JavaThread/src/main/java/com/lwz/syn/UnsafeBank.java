package com.lwz.syn;

public class UnsafeBank {
    public static void main(String[] args) {
        Account account = new Account(100,"买房钱");

        Drawing You = new Drawing(account,50,"你");
        Drawing Parent = new Drawing(account,100,"父母");

        You.start();
        Parent.start();
    }

}
//账户
class Account{
    int money;
    String name;

    public Account(int money,String name){
        this.money = money;
        this.name=name;
    }
}
//银行：模拟取钱
class Drawing extends Thread{
    Account account;//账户
    //取了多少钱
    int drawingMoney;
    //现在有多少钱
    int nowMoney;
    public Drawing(Account account,int drawingMoney,String name){
        super(name);
        this.account = account;
        this.drawingMoney = drawingMoney;
    }
    //取钱
    //synchronized 默认锁的是this。就是锁本类
    //保持线程同步要锁变化的类，在这个例子中也就是account，因为account进行了增和减
    @Override
    public  void run() {
       synchronized (account){
           //判断有没有钱
           if (account.money - drawingMoney < 0) {
               System.out.println(Thread.currentThread().getName()+"钱不够，取不了");
               return;
           }

           account.money = account.money - drawingMoney;
           nowMoney = nowMoney + drawingMoney;

           System.out.println(account.name+"余额为："+account.money);

           System.out.println(this.getName()+"手里的钱："+nowMoney);
       }
    }
}