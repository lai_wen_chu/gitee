package com.lwz.syn;

import java.util.concurrent.locks.ReentrantLock;

public class TestSynchronized {
    public static void main(String[] args) {
        TestSyn testSyn = new TestSyn();
        new Thread(testSyn,"小明").start();
        new Thread(testSyn,"小红").start();
        new Thread(testSyn,"黄牛党").start();
    }

}

class TestSyn implements Runnable{
   static int ticketNum = 2;

    private final ReentrantLock lock = new ReentrantLock();
    @Override
    public  void run() {
        try {
            lock.lock();
            if (ticketNum>0){
                System.out.println(Thread.currentThread().getName()+"拿到了第"+ticketNum--+"张票");
            }else {
                System.out.println(Thread.currentThread().getName()+"没抢到票");
            }
        }finally {
            lock.unlock();
        }
    }
}