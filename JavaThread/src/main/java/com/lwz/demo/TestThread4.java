package com.lwz.demo;
//模拟龟兔赛跑
public class TestThread4 implements Runnable{
    private static String Winner;
    public void run() {
        for (int i = 0; i <= 100; i++) {
            //模拟兔子睡觉
            if (Thread.currentThread().getName().equals("兔子")&&i%10==0){
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println(Thread.currentThread().getName()+"跑了"+i+"步");
            boolean flag = gameOver(i);
            if (flag) {
                break;
            }
        }
    }

    public boolean gameOver(int steps) {
        if (Winner != null) {
            return true;
        }else {
            if (steps == 100) {
                Winner=Thread.currentThread().getName();
                System.out.println("胜利者是："+Winner);
                return true;
            }
            return false;
        }
    }

    public static void main(String[] args) {
        TestThread4 testThread4 = new TestThread4();
        new Thread(testThread4,"兔子").start();
        new Thread(testThread4,"乌龟").start();
    }

}
