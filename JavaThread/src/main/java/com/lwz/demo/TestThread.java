package com.lwz.demo;
//实现多线程1：继承Thread类，重写run方法
public class TestThread extends Thread{
    @Override
    public void run() {
        for (int i = 0; i < 20; i++) {
            System.out.println("线程1====>"+i);
        }
    }

    public static void main(String[] args) {
        TestThread testThread=new TestThread();
        testThread.start();

        for (int i = 0; i < 1000; i++) {
            System.out.println("线程2====>"+i);
        }
    }
}
