package com.lwz.demo;
//实现多线程2：实现Runnable接口，重写run方法
public class TestThread2 implements Runnable {
    public void run() {
        for (int i = 0; i < 20; i++) {
            System.out.println("线程1====>"+i);
        }
    }

    public static void main(String[] args) {
        TestThread testThread=new TestThread();
        new Thread(testThread).start();

        for (int i = 0; i < 1000; i++) {
            System.out.println("线程2====>"+i);
        }
    }
}
