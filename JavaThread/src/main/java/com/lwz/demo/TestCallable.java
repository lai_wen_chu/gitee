package com.lwz.demo;

import java.util.concurrent.*;
//实现多线程3：实现callable接口，重写call方法
public class TestCallable implements Callable<Boolean> {

    public Boolean call() throws Exception {
        for (int i = 0; i < 20; i++) {
            System.out.println("线程1====>"+i);
        }
        return true;
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        for (int i = 0; i < 100; i++) {
            System.out.println("线程2====>"+i);
        }

        TestCallable t1=new TestCallable();
        //创建执行服务
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        //提交执行
        Future<Boolean> r1 = executorService.submit(t1);
        //获取结果
        boolean rs1 = r1.get();
        //关闭服务
        executorService.shutdownNow();
    }
}
