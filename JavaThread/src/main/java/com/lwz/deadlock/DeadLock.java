package com.lwz.deadlock;
//模拟死锁
//死锁：多个线程相互拥有对方需要的资源，然后形成僵持
public class DeadLock {
    public static void main(String[] args) {
        C c1 = new C(0,"you");
        C c2 = new C(1,"my");
        c1.start();
        c2.start();
    }
}
class A{

}
class B{

}

class C extends Thread{
    static A a = new A();
    static B b = new B();

    int flag;
    String name;
    C(int flag,String name){
        this.flag = flag;
        this.name = name;
    }

    @Override
    public void run() {
        try {
            deadLock();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void deadLock() throws InterruptedException {
        if (flag == 0) {
            synchronized (a){
                System.out.println(this.name+"得到A对象");
                Thread.sleep(1000);
                synchronized (b){
                    System.out.println(this.name+"得到B对象");
                }
            }
        }else {
            synchronized (b){
                System.out.println(this.name+"得到B对象");
                Thread.sleep(2000);
                synchronized (a){
                    System.out.println(this.name+"得到A对象");
                }
            }
        }

    }
}