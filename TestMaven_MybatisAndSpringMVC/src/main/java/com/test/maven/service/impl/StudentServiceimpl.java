package com.test.maven.service.impl;

import com.test.maven.dao.StudentDao;
import com.test.maven.entity.Student;
import com.test.maven.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service(value = "studentService")
public class StudentServiceimpl implements StudentService {
    @Autowired
    StudentDao std;
    @Override
    public void saveStudent(Student student) {
        std.saveStudent(student);
    }
}
