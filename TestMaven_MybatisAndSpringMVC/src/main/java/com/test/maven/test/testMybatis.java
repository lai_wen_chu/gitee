package com.test.maven.test;

import com.test.maven.dao.StudentDao;
import com.test.maven.entity.Student;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.jupiter.api.Test;

import java.io.InputStream;

public class testMybatis {
    @Test
    public void testMybatis() throws Exception{
        InputStream in = Resources.getResourceAsStream("mybatis-config.xml");
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(in);
        SqlSession sqlSession = factory.openSession(true);
        StudentDao studentDao = sqlSession.getMapper(StudentDao.class);
        Student student=new Student();
        student.setName("赖文卓");
        studentDao.saveStudent(student);
        sqlSession.close();
        in.close();
    }
}
