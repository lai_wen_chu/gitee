package com.test.maven.dao;

import com.test.maven.entity.Student;

public interface StudentDao {

    public void saveStudent(Student student);
}
