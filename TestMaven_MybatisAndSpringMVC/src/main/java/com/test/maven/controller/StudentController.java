package com.test.maven.controller;

import com.test.maven.entity.Student;
import com.test.maven.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


@Controller
@RequestMapping("/test")
public class StudentController {
    @Autowired
    StudentService studentService;
    @RequestMapping("/testSpringMVC")
    public String testSpringMvc(){
        System.out.println("执行了SpringMVC");
        return "success";
    }
    @RequestMapping(value = "/saveStudent",method = RequestMethod.POST)
    public ModelAndView saveStudent(Student student, Model model){
        System.out.println("执行了saveStudent...");
        studentService.saveStudent(student);
        model.addAttribute(student.getName());
        return new ModelAndView("success","name",model);
    }
}
