package com.test.maven.controller;

import com.alibaba.fastjson.JSONObject;
import com.test.maven.entity.Student;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/json")
public class HelloWorld {
    @GetMapping("/helloWord")
    public String helloWord(){
        System.out.println("你好世界");
        return "success";
    }
    @RequestMapping(value = "/testJson",produces = "application/json;charset=utf-8")
    @ResponseBody
    public String testJson(){
        Student student=new Student(14,"赖文卓");
//        String jsonString = JSONObject.toJSONString(student);
        return JSONObject.toJSONString(student);
    }

    @PostMapping(value = "/testFastJson")
    @ResponseBody
    public String testFastJson(@RequestBody Student student){
        System.out.println(student);
        return null;
    }
}
