package com.lwz.config;

import com.lwz.service.impl.CustomUserService;
import com.lwz.utils.MD5Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired//配置自定义登录成功处理
    MyAuthenticationSuccessHandler successHandler;
    @Autowired//配置自定义登录失败处理
    MyAuthenticationFailHandler failHandler;

    @Bean
    CustomUserService customUserService(){//注册UserDetailsService 的bean
        return new CustomUserService();
    }


    //链式编程
    //授权
    @Override
    protected void configure(HttpSecurity http) throws Exception {

        //首页所有人可以访问，功能也只有对应权限的人才能访问
        http.authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/level1/**").hasAuthority("ROLE_USER")
                .antMatchers("/level2/**").hasAuthority("ROLE_ADMIN")
                .antMatchers("/level3/**").hasAuthority("ROLE_ADMIN");
        //没有权限默认会到登录页面
        http.formLogin().loginPage("/toLogin").loginProcessingUrl("/login").successHandler(successHandler).failureHandler(failHandler);
        //注销 注销成功跳回首页
        http.csrf().disable();//注销异常时加上这句
        http.logout().logoutSuccessUrl("/");
        //记住我（功能）
        http.rememberMe().rememberMeParameter("remember");


    }


    //认证
    //密码编码：PasswordEncoder
    //在Spring Security 5.0+新增了很多的加密方法~
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        /*auth.userDetailsService(customUserService()).passwordEncoder(new PasswordEncoder(){
            @Override
            public String encode(CharSequence rawPassword) {
                return MD5Util.encrypt((String)rawPassword);
            }
            @Override
            public boolean matches(CharSequence rawPassword, String encodedPassword) {
                return encodedPassword.equals(MD5Util.encrypt((String)rawPassword));
            }});//user Details Service验证 使用md5加密，这个方法登录失败会报异常，暂时无法解决
            auth.userDetailsService(customUserService());*/
            //从数据库中进行账号密码验证
        auth.userDetailsService(customUserService()).passwordEncoder(new BCryptPasswordEncoder());
        //从内存中进行账号密码验证
//        auth.inMemoryAuthentication().passwordEncoder(new BCryptPasswordEncoder())
//                .withUser("laiwenzhuo").password(new BCryptPasswordEncoder().encode("123456")).roles("vip1","vip2")
//                .and()
//                .withUser("root").password(new BCryptPasswordEncoder().encode("123456")).roles("vip1","vip2","vip3")
//                .and()
//                .withUser("zhangsan").password(new BCryptPasswordEncoder().encode("123456")).roles("vip1");



    }
}
