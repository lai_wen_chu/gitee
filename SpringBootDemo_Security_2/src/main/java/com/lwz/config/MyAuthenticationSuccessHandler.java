package com.lwz.config;

import com.lwz.entity.User;
import com.lwz.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 自定义登录成功处理
 */
@Component
public class MyAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    public static final String RETURN_TYPE = "html";

    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    @Autowired
    UserMapper userMapper;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        //获取用户名
        String username = request.getParameter("username");
        //通过用户名查找用户信息
        User user= userMapper.findUserByUname(username);
        System.out.println(user);
        if (RETURN_TYPE.equals("html")){
            //把用户信息存进session里方便前端显示
            request.getSession().setAttribute("user",user);
            request.getRequestDispatcher("index").forward(request,response);
//            redirectStrategy.sendRedirect(request,response,"index");
        }
    }
}
