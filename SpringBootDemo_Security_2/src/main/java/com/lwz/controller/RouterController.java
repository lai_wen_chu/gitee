package com.lwz.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class RouterController {

    @RequestMapping({"/","/index"})
    public String toIndex(){
        return "index";
    }

    @RequestMapping("/toLogin")
    public String toLogin(){
        return "login";
    }

    @RequestMapping("/level1")
    public String level1(){
        return "views/level1/1";
    }
    @RequestMapping("/level2")
    public String level2(){
        return "views/level2/1";
    }
    @RequestMapping("/level3")
    public String level3(){
        return "views/level3/1";
    }

}
