package com.lwz.entity;

import java.util.List;

public class User {
    private int id;
    private String username;
    private String password;
    private  String perms;
    private List<Role> roles;
    public User() {
    }

    public User(int id, String username, String password, String perms, List<Role> roles) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.perms = perms;
        this.roles = roles;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPerms() {
        return perms;
    }

    public void setPerms(String perms) {
        this.perms = perms;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", perms='" + perms + '\'' +
                ", roles=" + roles +
                '}';
    }
}
