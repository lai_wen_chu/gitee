package com.lwz.mapper;


import com.lwz.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


@Repository
public interface UserMapper {
    @Autowired

    public User findUserByUname(String name);

}
