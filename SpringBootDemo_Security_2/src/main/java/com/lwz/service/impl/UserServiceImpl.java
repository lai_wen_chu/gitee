package com.lwz.service.impl;

import com.lwz.entity.User;
import com.lwz.mapper.UserMapper;
import com.lwz.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserServiceImpl implements UserService {
    @Autowired
    UserMapper userMapper;
    @Override
    public User findUserByUname(String name) {
        return userMapper.findUserByUname(name);
    }
}
