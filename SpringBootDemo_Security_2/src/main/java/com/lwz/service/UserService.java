package com.lwz.service;


import com.lwz.entity.User;

public interface UserService {
    public User findUserByUname(String name);
}
