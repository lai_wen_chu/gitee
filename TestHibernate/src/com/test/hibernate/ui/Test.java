package com.test.hibernate.ui;

import com.test.hibernate.entity.ClassName;
import com.test.hibernate.entity.Student;
import com.test.hibernate.utils.HibernateUtil;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.List;

public class Test {
    public static void main(String[] args) {
//        Test.save();
//        Test.save1();
//        Test.deleteClass(4);
        List<Student> students = findAllStudnet();
        for (Student student:students){
            System.out.println(student.getName());
        }
    }
    public static void save(){
        Configuration cfg=new Configuration().configure();
        SessionFactory sessionFactory = cfg.buildSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        ClassName className=new ClassName();
        className.setName("软件工程一班");
        Student student=new Student();
        student.setName("zhangsan");
        student.setClassName(className);
        Student student1=new Student();
        student1.setName("wangwu");
        student1.setClassName(className);
        className.getStudents().add(student1);
        className.getStudents().add(student);
        session.save(className);
        session.save(student);
        transaction.commit();
        session.close();
        sessionFactory.close();
    }
    public static void save1(){
        Session session=null;
        Transaction transaction=null;
        session= HibernateUtil.openSession();
        transaction=session.beginTransaction();
        ClassName className=new ClassName();
        className.setName("软件工程一班");
        Student student=new Student();
        student.setName("zhangsan");
        student.setClassName(className);
        Student student1=new Student();
        student1.setName("wangwu");
        student1.setClassName(className);
        className.getStudents().add(student1);
        className.getStudents().add(student);
        session.save(className);
        session.save(student);
        transaction.commit();
        session.close();
    }
    public static void deleteClass(int cid){
        Configuration configure = new Configuration().configure();
        SessionFactory sessionFactory = configure.buildSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        ClassName className = session.load(ClassName.class, cid);

        session.delete(className);
        transaction.commit();
        session.close();
    }

    public static List<Student> findAllStudnet(){
        Session session=null;
        Transaction transaction=null;
        try {
            session=HibernateUtil.openSession();
            transaction=session.beginTransaction();
            Query query = session.createQuery("from Student");
            List<Student> students=query.list();
            return students;
        }catch (Exception e){
            e.printStackTrace();
            transaction.rollback();
            return null;
        }finally {
            session.close();
        }

    }

}
