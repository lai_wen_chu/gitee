package com.test.hibernate.entity;


import java.util.HashSet;
import java.util.Set;

public class
ClassName {
    private int id;
    private String name;
    private Set<Student> students=new HashSet<Student>();

    public ClassName() {
    }

    public ClassName(String name, Set<Student> students) {
        this.name = name;
        this.students = students;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Student> getStudents() {
        return students;
    }

    public void setStudents(Set<Student> students) {
        this.students = students;
    }

    @Override
    public String toString() {
        return "ClassName{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", students=" + students +
                '}';
    }
}
