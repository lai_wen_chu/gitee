package com.spring.security.mapper;

import com.spring.security.entity.User;
import org.springframework.stereotype.Repository;


@Repository
public interface UserMapper {

    public User findUserByUname(String name);

}
