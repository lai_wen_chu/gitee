package com.spring.security.service;

import com.spring.security.entity.User;

public interface UserService {
    public User findUserByUname(String name);
}
