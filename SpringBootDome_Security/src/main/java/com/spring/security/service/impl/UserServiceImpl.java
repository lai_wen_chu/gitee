package com.spring.security.service.impl;

import com.spring.security.entity.User;
import com.spring.security.mapper.UserMapper;
import com.spring.security.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserMapper userMapper;
    @Override
    public User findUserByUname(String name) {
        return userMapper.findUserByUname(name);
    }
}
