package com.lwz.service;

import com.lwz.entity.User;

public interface UserService {
    public User findUserByName(String username);
}
