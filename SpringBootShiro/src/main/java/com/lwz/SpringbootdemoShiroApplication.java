package com.lwz;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.lwz.mapper")
public class SpringbootdemoShiroApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootdemoShiroApplication.class, args);
    }

}
