package com.lwz.mapper;

import com.lwz.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
public interface UserMapper {
    public User findUserByName(String username);
}
