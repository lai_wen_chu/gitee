package com.lwz.config;

import com.lwz.entity.User;
import com.lwz.service.UserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;

public class UserRealm extends AuthorizingRealm {
    @Autowired
    UserService userService;

    //授权
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        System.out.println("执行了=>授权doGetAuthorizationInfo");
        SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
        //添加授权
        //authorizationInfo.addStringPermission("user:add");

        //拿到当前用户对象
        Subject subject = SecurityUtils.getSubject();
        //获取user对象
        User principal = (User) subject.getPrincipal();
        authorizationInfo.addStringPermission(principal.getPerms());
        return authorizationInfo;
    }

    //认证
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        System.out.println("执行了=>认证doGetAuthenticationInfo");

        UsernamePasswordToken userToken=(UsernamePasswordToken)token;
        //连接真实数据库
        User user = userService.findUserByName(userToken.getUsername());
        if(user==null){//为null则return:null
            return null;//抛出异常UnknownAccountException
        }
        Subject currentSubject = SecurityUtils.getSubject();
        Session session = currentSubject.getSession();
        session.setAttribute("user",user);
        //密码认证Shiro帮做
        //存放user对象
        return new SimpleAuthenticationInfo(user,user.getPassword(),"");
    }
}
