package com.lwz.swaggerdemo.controller;

import com.lwz.swaggerdemo.entity.User;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    //添加方法的注释
    @ApiOperation("你好方法")
    @GetMapping("/hello")
    public String hello(){
        return "你好世界";
    }

    //只要我们的接口中，返回值中存在实体类，他就会被扫描到Swagger中
    @PostMapping("/user")
    @ApiOperation("user方法")
    public User helloUser(User user){
        return user;
    }
}
