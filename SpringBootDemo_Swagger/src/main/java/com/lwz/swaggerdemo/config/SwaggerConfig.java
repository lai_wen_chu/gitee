package com.lwz.swaggerdemo.config;


        import org.springframework.context.annotation.Bean;
        import org.springframework.context.annotation.Configuration;
        import org.springframework.core.env.Environment;
        import org.springframework.core.env.Profiles;
        import springfox.documentation.builders.RequestHandlerSelectors;
        import springfox.documentation.service.ApiInfo;
        import springfox.documentation.service.Contact;
        import springfox.documentation.spi.DocumentationType;
        import springfox.documentation.spring.web.plugins.Docket;
        import springfox.documentation.swagger2.annotations.EnableSwagger2;

        import java.util.ArrayList;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket docket1(){
        return new Docket(DocumentationType.SWAGGER_2).groupName("A");
    }

    @Bean
    public Docket docket2(){
        return new Docket(DocumentationType.SWAGGER_2).groupName("B");
    }

    @Bean
    public Docket docket3(){
        return new Docket(DocumentationType.SWAGGER_2).groupName("C");
    }

    //配置Swagger的Docket的bean实例
    @Bean
    public Docket docket(Environment environment) {
        //设置显示swagger的环境，监听application.yml如果激活的是dev或test配置文件则返回true，否则返回false
        //注意配置文件端口的变化
        Profiles profiles = Profiles.of("dev","test");
        boolean flag = environment.acceptsProfiles(profiles);
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .groupName("XXX")//设置组名
                .enable(flag)//enable是否启动swagger，如果是false则不能在网页上访问swagger
                .select()
                //RequestHandlerSelectors：配置要扫描的接口方法
                //basePackage：指定要扫描的包
                //none:不扫描
                //any:扫描全部
                //withClassAnnotation:扫描类上的注解
                //withMethodAnnotation:扫描方法上的注解
                .apis(RequestHandlerSelectors.basePackage("com.lwz.swaggerdemo.controller"))
                //paths()过滤扫描路径
                .build();

    }

    //配置Swagger信息apiInfo
    private ApiInfo apiInfo() {
        Contact contact = new Contact("XXX","https://www.bilibili.com/index.html","939296345@qq.com");
        return new ApiInfo(
                "XXX的swagger",
                "这是一个描述",
                "V1.0",
                "https://www.bilibili.com/index.html",
                contact,
                "Apache 2.0",
                "http://www.apache.org/licenses/LICENSE-2.0",
                new ArrayList());
    }
}
