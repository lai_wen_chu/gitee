package com.lwz.swaggerdemo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SwaggerDemoApplicationTests {
    @Value("${spring.profiles.active}")
    public String evm;
    @Test
    public void contextLoads() {
        System.out.println(evm);
    }

}
