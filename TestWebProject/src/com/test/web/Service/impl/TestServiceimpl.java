package com.test.web.Service.impl;

import com.test.web.Dao.AdminDao;
import com.test.web.Service.TestService;
import com.test.web.entity.Admin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("testService")
public class TestServiceimpl implements TestService {
    @Autowired
    AdminDao adminDao;
    @Override
    public List<Admin> findAllAdmin() {
        List<Admin> admins = adminDao.findAllAdmin();
        return  admins;
    }
}
