package com.test.web.Service;

import com.test.web.entity.Admin;

import java.util.List;

public interface TestService {
    public List<Admin> findAllAdmin();
}
