package com.test.web.Dao;

import com.test.web.entity.Admin;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface AdminDao {
//    @Select("select * from admin")
    public List<Admin> findAllAdmin();
//    @Insert("insert into admin (name,password) value(#({name},#{password})")
    public void saveAdmin(Admin admin);
}
