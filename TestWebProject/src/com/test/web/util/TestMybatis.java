package com.test.web.util;

import com.test.web.Dao.AdminDao;
import com.test.web.entity.Admin;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.InputStream;
import java.util.List;

public class TestMybatis {
    public static void main(String[] args) throws Exception {
        InputStream in = Resources.getResourceAsStream("mybatis-config.xml");
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(in);
        SqlSession session = factory.openSession(true);
        AdminDao adminDao = session.getMapper(AdminDao.class);
        List<Admin> allAdmin = adminDao.findAllAdmin();
        for (Admin admin:allAdmin){
            System.out.println(admin);
        }
//        Admin admin=new Admin();
//        admin.setName("王五");
//        admin.setPassword("123456");
//        adminDao.saveAdmin(admin);
        session.close();
        in.close();
    }
}
