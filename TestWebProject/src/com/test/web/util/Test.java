package com.test.web.util;

import com.test.web.Service.TestService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {
    public static void main(String[] args) {
        ApplicationContext ac=new ClassPathXmlApplicationContext("classpath:ApplicationContext.xml");
        TestService ts = (TestService) ac.getBean("testService");
        ts.findAllAdmin();
    }
}
