package com.test.web.Controller;

import com.test.web.Service.TestService;
import com.test.web.entity.Admin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/test")
public class TestController {
    @Autowired
    TestService ts;

    @RequestMapping("/test")
    public String test(Model model){
        model.addAttribute("hh","你好啊");
        ts.findAllAdmin();
        return "success";
    }

    @RequestMapping(value = "/findAll")
    public String findAll(Model model){
        List<Admin> admins = ts.findAllAdmin();
        for (Admin admin:admins){
            System.out.println(admin);
        }
        model.addAttribute("admins",admins);
        return "success";
    }
}
