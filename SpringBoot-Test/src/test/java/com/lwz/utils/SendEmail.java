package com.lwz.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Component
public class SendEmail {
    @Autowired
    JavaMailSenderImpl javaMailSender;



    /**
     *
     * @param Subject :主题
     * @param Text :正文
     * @param setTo :发送给谁（邮箱号）
     * @param setFrom :你的邮箱
     * @return
     * @throws MessagingException
     */
    public MimeMessage sendMail(String Subject,String Text,String setTo,String setFrom) throws MessagingException {
        //复杂邮件
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();

        //组装
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage,true);//为true表示开启上传
        helper.setSubject(Subject);//设置主题
        helper.setText(Text,true);//为true表示开启html样式
        //添加附件
//        helper.addAttachment("xxxx.docx",new File("C:\\Users\\HASEE\\Desktop\\xxx.docx"));
        helper.setFrom(setFrom);
        helper.setTo(setTo);
        javaMailSender.send(mimeMessage);
        return mimeMessage;
    }

    /**
     *
     * @param Subject :主题
     * @param Text :正文
     * @param setTo :发送给谁（邮箱号）
     * @param setFrom :你的邮箱
     * @return
     * @throws MessagingException
     */
    public MimeMessage getSendMail(String Subject,String Text,String setTo,String setFrom) throws MessagingException {

        return sendMail(Subject,Text,setTo,setFrom);
    }

}
