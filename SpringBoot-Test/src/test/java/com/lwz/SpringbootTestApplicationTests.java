package com.lwz;

import com.lwz.utils.SendEmail;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;


@SpringBootTest
class SpringbootTestApplicationTests {

    @Autowired
    JavaMailSenderImpl javaMailSender;
    @Autowired
    SendEmail sendEmail;

    //一个简单的邮件
    @Test
    void contextLoads() {

        SimpleMailMessage message = new SimpleMailMessage();
        message.setSubject("XXXX你好");//设置主题
        message.setText("upup");//正文内容

        message.setTo("939296345@qq.com");//收件人
        message.setFrom("laiwenzhuo@126.com");//寄件人

        javaMailSender.send(message);
    }


    @Test
    void contextLoads2() throws MessagingException {
        //复杂邮件
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        //组装
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage,true);//为true表示开启上传
        helper.setSubject("赖文卓你好");//设置主题
        helper.setText("<p style='color:red'>这是我的简历</p>",true);//为true表示开启html样式

        //添加附件
        helper.addAttachment("赖文卓.docx",new File("C:\\Users\\HASEE\\Desktop\\赖文卓.docx"));
        helper.setTo("939296345@qq.com");
        helper.setFrom("laiwenzhuo@126.com");
        javaMailSender.send(mimeMessage);
    }
    @Test
    void contextLoads3() throws MessagingException {
        sendEmail.getSendMail("第二次测试封装邮箱类","测试啊","939296345@qq.com","laiwenzhuo@126.com");
    }


}
