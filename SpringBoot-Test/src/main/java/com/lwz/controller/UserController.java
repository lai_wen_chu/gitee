package com.lwz.controller;

import com.lwz.service.AsyncService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
    @Autowired
    AsyncService asyncService;

    @GetMapping("hello")
    public String hello(){
        asyncService.hello();
        return "OK";
    }

}
