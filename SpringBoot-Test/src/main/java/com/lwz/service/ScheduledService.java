package com.lwz.service;

import com.lwz.utils.SendEmail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;

@Service
public class ScheduledService {
    @Value("${spring.mail.username}")
    String from;


    @Autowired
    SendEmail sendEmail;
    //*是通配符。就是如果天的位置配置了*就代表每天，月配置了*就代表每月
    //             秒、分、小时、哪天、哪个月、星期几
    @Scheduled(cron = "0 20 18 * * ?")
    public void hello(){
        System.out.println("执行了");
    }
//             秒、分、小时、哪天、哪个月、星期几
    @Scheduled(cron = "0 * * * * ?")
    public void mail() throws MessagingException {
        System.out.println(from);
        sendEmail.getSendMail("哈哈哈测试定时任务","测试测试","939296345@qq.com",from);
    }
}
