package com.test.springboot;

import com.test.springboot.entity.Config;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
 class  SpringbootdemoApplicationTests {
    static int i;
    Double aDouble;

    @Autowired
    Config config;
    @Test
    public void test(){
        System.out.println(config);
    }

    @Test
    public void test1(){
        Long l=2L;
        int num = 2147483647;
        num+=l;
        System.out.println() ;
    }

    @Test
    public void test3(){
        int a=9;
        switch (a){
            case 0 :
                System.out.println("0");
            case 1 :
                System.out.println("1");
            case 2:
                System.out.println("2");
        }
    }
    static int a=6;
    int b=6;
    @Test
    public void test4(){
        a=5;
        b=5;
        System.out.println(b);
    }
    @Value("${spring.profiles.active}")
    String name;
    @Test
    public void test5(){
        boolean flag;
        System.out.println(name);
        System.out.println(flag=name.equals("dev")||name.equals("lai")?true:false);
    }
}
