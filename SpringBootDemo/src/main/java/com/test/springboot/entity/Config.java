package com.test.springboot.entity;


import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
//读取配置文件
@ConfigurationProperties(prefix = "test")
public class Config {
private String name;
private String location;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "Config{" +
                "name='" + name + '\'' +
                ", location='" + location + '\'' +
                '}';
    }
}
