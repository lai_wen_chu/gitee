package com.test.springboot.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Arrays;

@Controller
@RequestMapping("Index")
public class IndexController {

    @GetMapping("index")
    public ModelAndView index(){
        return new ModelAndView("index","hello","hello,world");
    }

    @GetMapping("/testeach")
    public String testeach(Model model){
        model.addAttribute("hello", "hello,world");
        model.addAttribute("users", Arrays.asList("张三","赵四","王五"));
        return "success";
    }
}
