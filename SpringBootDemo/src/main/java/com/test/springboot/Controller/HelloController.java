package com.test.springboot.Controller;

import com.test.springboot.entity.Config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HelloController {
    @Value("${test.name}")
    private String name;
    @Value("${test.location}")
    private String location;
    @Autowired
    Config config;

    @RequestMapping(value = "/test")
    public @ResponseBody String test() {

        return "你好，世界";
    }

    //读取配置文件
    //@RequestMapping(value = "/configInfo")
    @GetMapping(value = "/configInf")
    public @ResponseBody String configInfo() {

        return name + "-----" + location + "-----" + config.getName() + "-----" + config.getLocation();
    }


}
