package com.springboot.demo.Config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
@Configuration
public class MyConfig implements WebMvcConfigurer {
    @Override
    //自定义视图解析器
    public void addViewControllers(ViewControllerRegistry registry) {
        //addViewController：/**访问什么路径时跳转至setViewName：xxx
        registry.addViewController("/").setViewName("pages-signin");
        registry.addViewController("/login.html").setViewName("pages-signin");
        registry.addViewController("/success.html").setViewName("success");

    }

    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new LoginHandlerInterceptor()).
                addPathPatterns("/**").
                excludePathPatterns("/login.html","/","/user/login"
                        ,"/ajax/**","/images/**","/javascripts/**","/stylesheets/**","/vendor/**");
    }
}
