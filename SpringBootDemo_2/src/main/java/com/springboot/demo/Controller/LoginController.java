package com.springboot.demo.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/user")
public class LoginController {
    @RequestMapping("/login")
    public String Login(
            @RequestParam("username") String username,
            @RequestParam("pwd") String password,
            Model model, HttpServletRequest request){
        if (!StringUtils.isEmpty(username) && password.equals("123456")){
            //登录成功设置session
            request.getSession().setAttribute("username",username);
            return "redirect:/success.html";
        }else {
            //登录失败，提示信息
            model.addAttribute("msg", "用户名密码错误！");
            return "pages-signin";
        }
    }
}
