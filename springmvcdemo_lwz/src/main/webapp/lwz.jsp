<%--
  Created by IntelliJ IDEA.
  User: HASEE
  Date: 2020/3/30
  Time: 12:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<html>
<head>
    <title>测试json</title>
</head>
<script type="text/javascript" src="js/jquery-3.4.0.min.js"></script>
<script type=text/javascript>

    function testJson() {
        var info={name:$("#name").val(),address:$("#address").val(),date:$("#date").val()}
        $.ajax({
            url:"Test/FastJson",
            type:"post",
            dataType:"json",
            contentType:"application/json;charset=UTF-8",
            data:JSON.stringify(info),
            success:function (data) {
                alert("姓名："+data.name+"   "+"地址："+data.address+" "+"日期："+data.date);
                console.log(data)
            }
        })
    }
</script>
<body>
姓名：<input type="text" id="name" ><br>
住址：<input type="text" id="address"><br>
生日：<input type="text" id="date"><br>
<input type="button" value="提交" onclick="testJson()">


<form action="Test/login" method="get">
用户名：<input type="text" name="username"><br>
密码：<input type="text" name="password">
    <input type="submit" value="登录">
</form>

<h2>测试日期格式转换器</h2>
<form action="Test/testDate" method="post">
    姓名：<input type="text" name="name" ><br>
    住址：<input type="text" name="address"><br>
    工资：<input type="text" name="wages"><br>
    生日：<input type="text" name="date"><br>
    <input type="submit" value="提交"><br>
</form>

<h2>测试对象格式转换器</h2>
<form action="Test/testInfo" method="post">
    个人信息：<input type="text" name="information">
    <input type="submit" value="提交">
</form>
</body>
</html>
