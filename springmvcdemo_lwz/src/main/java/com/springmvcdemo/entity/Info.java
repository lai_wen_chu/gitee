package com.springmvcdemo.entity;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;

import javax.annotation.Resource;
import java.util.Date;

public class Info {
    private String name;
    private String address;
    @NumberFormat(style = NumberFormat.Style.NUMBER,pattern = "##,##")
    private int wages;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date date;

    public Info() {
    }

    public Info(String name, String address, int wages, Date date) {
        this.name = name;
        this.address = address;
        this.wages = wages;
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getWages() {
        return wages;
    }

    public void setWages(int wages) {
        this.wages = wages;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Info{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", wages='" + wages + '\'' +
                ", date=" + date +
                '}';
    }
}
