package com.springmvcdemo.entity;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.*;
import java.util.Date;

public class Student {
    @NotBlank(message = "名字不能为空")
    @Pattern(message = "请输入长度大于3,小于20",regexp = "^.{3,20}$")
    private String name;
    @NotBlank(message = "邮箱不能为空")
    @Email(message = "请输入正确的邮箱格式")
    private String email;
//    @NotNull(message = "年龄不能为空")
//    @Size(min = 0)
    @NotBlank(message = "该值不能为空")
//    @NotEmpty(message = "该值不能为空")
    @Range(min =1 ,max =120 ,message = "请输入年龄在1~120之间")
//    @Min(value = 1,message = "年龄不能小于1")
//    @Max(value = 120,message = "年龄不能大于120")
    private String age;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @NotNull(message = "日期不能为空")
    @Past(message = "日期必须是一个过去的日期")
    private Date date;

    public Student() {
    }

    public Student(String name, String email, String age, Date date) {
        this.name = name;
        this.email = email;
        this.age = age;
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", age='" + age + '\'' +
                ", date=" + date +
                '}';
    }
}
