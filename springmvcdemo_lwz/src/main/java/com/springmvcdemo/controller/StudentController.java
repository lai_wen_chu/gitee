package com.springmvcdemo.controller;

import com.springmvcdemo.Validator.Validator;
import com.springmvcdemo.entity.Student;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Map;

@Controller
@RequestMapping("/lwz")
public class StudentController {

    @PostMapping("/test")
    public String test(@Valid Student student, BindingResult result, Model model) {
        Map<String, Object> map = Validator.getValidator(result);
        if (map == null) {
            //说明没有错误
            model.addAttribute("student",student);
            return "/success.jsp";
        }else {
            System.out.println(student);
            //存在错误信息，获取错误信息，实现跳转
            model.addAttribute("error",map);
            model.addAttribute("student",student);
//            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//            String date = dateFormat.format(student.getDate());
//            model.addAttribute("date",date);
            return "/student.jsp";
        }
    }
}
