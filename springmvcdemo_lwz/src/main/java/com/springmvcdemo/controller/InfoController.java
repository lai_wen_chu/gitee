package com.springmvcdemo.controller;

import com.alibaba.fastjson.JSON;
import com.springmvcdemo.entity.Info;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;


@Controller
@RequestMapping("/Test")
public class InfoController {

    @PostMapping(value = "/FastJson",produces = "application/json;charset=utf-8")
    @ResponseBody
    public String testFastJson(@RequestBody Info info){
        String string = JSON.toJSONString(info);
        System.out.println(string);
        Info info1 = JSON.parseObject(string, Info.class);
        System.out.println(info1);
        return string;
    }

//    @GetMapping("/login")
    @RequestMapping(value = "/login",method = RequestMethod.GET)
    public String login(String username, String password, HttpServletRequest request){
        System.out.println(username);
        if (username.equals("admin") && password.equals("123456")){
            request.getSession().setAttribute("user",username);
            return "redirect:/success.jsp";
        }else {
            return "redirect:/lwz.jsp";
        }

    }
    @PostMapping("/testDate")
    @ResponseBody
    public Info testDate(Info info){
        System.out.println(info);
        return info;

    }
    @PostMapping("/testInfo")
    @ResponseBody
    public Info testInfo(@RequestParam("information") Info info){
        System.out.println(info);
        return info;
    }

}
