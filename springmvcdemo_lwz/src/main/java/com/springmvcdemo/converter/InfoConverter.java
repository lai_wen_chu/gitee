package com.springmvcdemo.converter;

import com.springmvcdemo.entity.Info;
import org.springframework.core.convert.converter.Converter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class InfoConverter implements Converter<String, Info> {
    @Override
    public Info convert(String s) {
        String[] strings = s.split("/");

        Info info = new Info();
        info.setName(strings[0].replaceAll(" ",""));
        info.setAddress(strings[1].replaceAll(" ",""));
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date date = simpleDateFormat.parse(strings[2]);
            info.setDate(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return info;
    }
}
