package com.springmvcdemo.Validator;

import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.HashMap;
import java.util.Map;

public class Validator {

    public static Map<String,Object> getValidator(BindingResult result){
        Map<String,Object> map=null;
        boolean errors = result.hasErrors();
        if (errors){
            map = new HashMap<String, Object>();
            for (FieldError error : result.getFieldErrors()) {
                String field = error.getField();
                String message=error.getDefaultMessage();
                map.put(field, message);
            }
            return map;
        }
        return null;
    }
}
